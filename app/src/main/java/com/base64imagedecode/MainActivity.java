package com.base64imagedecode;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {

    private String responseFromServer="";

    ImageView aadhaarProfileImage;
    TextView jsonDataTv;

    private String name;
    private String dob;
    private String gender;
    private String phone;
    private String email;

    private String co;
    private String house;
    private String street;
    private String lm;
    private String loc;
    private String vtc;
    private String subdist;
    private String dist;
    private String state;
    private String country;
    private String pc;
    private String po;
    private String vtcCode;

    private String pht;
    private String prn;
    private String uid;

    private int lLang;
    private String lName;
    private String lCo;
    private String lHouse;
    private String lStreet;
    private String lLm;
    private String lLoc;
    private String lVtc;
    private String lSubdist;
    private String lDist;
    private String lState;
    private String lPc;
    private String lPo;

    private String languageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        aadhaarProfileImage = (ImageView)findViewById(R.id.aadhaar_profile_image);
        jsonDataTv = (TextView) findViewById(R.id.json_data);

        responseFromServer = "{ \"aadhardtls\": {   \"poi\": {\"name\": \"Umesh Shyamlal Chauhan\",    \"dob\": \"30-05-1991\",    \"gender\": \"M\",     \"phone\": null,     \"email\": null },    \"poa\": {       \"co\": null,       \"house\": \"Devman Tiwari Chawl\",      \"street\": \"S.V.Road\",        \"lm\": \"Near MTNL\",    \"loc\": \"Vasri Hill, Dongri\",      \"vtc\": \"Goregaon West\",    \"subdist\": null, \"dist\": \"Mumbai\",   \"state\": \"Maharashtra\",  \"country\": \"India\",     \"pc\": \"400104\",       \"po\": null,         \"vtcCode\": null },  \"pht\": \"/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADIAKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDqcUuKUUooAQCnAUUtABijFFLTAMUYpadkBSSQABkmgBuKXbTRNE0fmCVDGRneGGMfWpAQQCCCCMg+ooATFGKdxmikAmKMU7FGKAG4oxTqKAG4pMU6igBmKTFPooArCnClxS4oAQUtFKBTAKXFAFVNR1aw0i2M99cpDH7gkkZAOAMk4LDOBxmgC2SB1IH1qnearZWTbJbq2jm27gsswQY9T6flXl/iH4j6jfExabmxt+RnjzWHuf4ex+XkepFcbNcT3kzS3E0s0rHLO5Lsx6ZJPJNAHrOr+P8AT7WC4S2vkkm3ER+VGVZPTO4FWHuDz6d65yy8cXzSMZvEMMQB3FpbDJb2IReemM/l7cG6FW2qHLdcYqLzSpIHy/hikB7PZeNpoty6nDby28eN9/ZSb4wSeMrkkcFe5OeMZIFdjb3MVzEskTq6soYMpyGB6EEdRXzjp+pz6ddpcQPtIJypGVYEEFSOhBBIIPY13Xh/xqmlwvFOheKU5gVTt25JySTgYz1b3PB5pgetUtedR/E2M+Xm0gDFwGH2p8AY6/6vnniuj0fxjYa1MIbZSsp48qRgrscn7o6EYBbg5wOlFgOhopwwwBByCM0YoAZRTsUmKQCUlOpMUAVxTqQCloAKUUYrL8Q6xFoWiXF9Iyh1XbCrc75CPlGM888nHYE9qYGT4u8aQ+H4zbWoSfUWH3GPyxDGQWx3OQQPTk44z49qOqXWo3bXN5cvPO/LOx6/TsBzwBgDsBUd3M9zcO7b3lkYs3OSSTkknuSSSajddsaqVQH0A5/EmkBGJCx6de5xTzI2CoY57bQKi+U+59hTxIhQr5LZH8Q6UDIg8qSlxNsYcgnP9Ka83mn5wpPqKmaVJIwrYJHt2qPyUdcgj6Z5/KgRCd4zjp7UCZgRz0p4jwe9MZCOeABQBMrsx++BVuOWVAAXyGqgmD1GR7GrPllApjOQRkBjQB6T4O8ffZJBYazO7QO5ZLt2JZGJJIcnqpJzu6g5zkH5fVFYMoYHIPIyMV8yhnKngAfyr0v4d+MW82PQNQP3vltZchQuB9w/UDg+vHcYAPT6SnYpMUAJRS4pKAK9LSUtADq8h+Juqtc6+tiGIjsowAvH33AYn8to9sH1r1qRxHGWY7VHU+lfPeqag+r63c38xOJZC+COVX+FfwUAfhQBRJCjLZz6ZqvJc5kJGce1TO7PnbjLfoKiEIBAyGJ7DtQMbEJJW+UdPariLc7skkZ71f0+3Krz0PYVrpACuNox9KxlUtsbRpXWpzP2IOxIYDuTjFN+y5JIYn3rpnsWbj5Rk8k8YqvFpciygE4Rjxzz/wDqpe00KdIxYrKRkHAOfemy2bGIuB+FdSdNCDYOvcjqaS5slWAKABgcCp9o7j9locWImBPl8nHKmrBkUQo6H5DwyZ6GrdxbCKUgr0NU54kU/JkHvnvW8ZXRzyi0PJ2LtweehzQrvA8c0TssiEMroxBUjoQexBqMAmA55x+lPibK/N071RJ9AeD9cfXtAjnnIN1C3kTkLtDsACHAwOGVlboBkkDpW9Xlvwr1I/bLnTWVz/o+5XJG0Kj52gY9ZmJ65yPQV6jQAhpKWkoArinUzNOFAGH40uJLbwdqbxY3NEIzn+67BG/RjXgqnJK9znmvePGgz4P1LIc/uwcKQD94fp6+1eCb9smfSmA7BJ2g4J4NWbS3XcGPeqofeS2MH19q0bD52BqJOyLgrs17eMAdMCtGLC471VgjJwAK0oLbpu71xyZ3RQ5IQw3c5P5CpokVSWCc9M1bhs9y8dqc1m44waLsehSlQtyBtNVJEyK1fsj9xUcsChc9al3GrHL3UIkz8uDWNNCd5Vh6gGuvuLdD3xWTeWgdCAQw/WtoSsY1IpnNNmNiuc5H5UxSRnPQ1LOrRTbX7dDUOflI7V0p9TjasehfCli3iGdfMIKWrHbt+8Cyg89ui/X8K9h614p8KpAviq4z/wA+Lj/yJHXs6tkU7gPNFJmigCsBTgKaDTqBHOePpGi8E6iVJBIjXPoDIoP5jj8a8FbgH619Ha5aw3uiXdvPBJPG0ZJij+85HIA98gYr52vLZra4mt5cCSJyjY9QcGgCJeQqDq3Wt21VowBFGXcD8KxIV/0qMetdlCFtrZfl5xk8VlUdjalG5SU6qoLLDn2yKa+ralbOC6N9NlX0vJ5Y5WijJWJSzlTgLjnk/wCAqrFqxvp44GslzI4RGLdSTj05pRTfQ0lZbSLNn4pu2YIYgFPBJB4roP7eJs1baM4yRiufk054XlGwpJEfnibqO2fce44rQ04ROCkidRWc3boaQT7kkuuXDxbljCjGc+tc7d63qDEjeR7gCtvVlWMBIwcdqq2GnCZosFGnlb5Vc4AA7n0pwlcJxZjj+0bjDPM8SnnLnANWV0pzhmvNx9VFSapdajY3c9i8xRo5QqRpFgOvPOevPGB7+1Xb/T5oIoJA++VkBkjfG5CR0DDr+NaST6GUeV+Zh6tpzQ23m+ZvAPXuKxP4c12cls91YSxMDkrxn1rj2jZCyMMHNFN3RnVjZ6HYfDE7fE87d/sT8f8AbSOvZ7d8gV5b8OLRTFPfYG5T9nHygEDhzzjnJbv6D0r061bgVV9SLaF0ciikByKWrEVBTxTBT6AF7V4j4/8AD50nxDLNDEEs7vMsW3oG/jHtzzgcYYV7dXLeO7dZ9CRvI8wpMCW/uKVYE/ido/EUPRDSu7Hi+nW/m6jCP9oV232RZl2sOO9YUdslnrEOzIR2+X+VdZCF2jJrmqu6TOmlGzaKUMc1luFuBtPVcAg/Wq9np8VreC5ht/LlUkhs8D6A5xW0EBPSp9ionAG41EZvubuCMy6VWYSFD5pHLlmJI/Emq9pEQ+fWtG5TbExPLHvVW2jct8vp1qZPm1KSsGoQ5jDelRWRwv3VYDjkcirstrK0eCRyM/WqtopimKkcGlF2QSWpNJbSso2MoUcjCAfyxTY7M7gztkjtV1MDp0NKwVRnpTlJsOUrSQ47Vx2p2BbUnjjA5YH8D/8ArrspbgDA4rm7lt+sz44Cx/0q6baTMKkbtJnYeC4HtbC4ifqLgkYHGMAdfXj8sV29qeBXJ+GI3XTDIzhvNlZxjt0Uj81J/Gustelaw1MJpJ6F1TxS0i8CnVsZFMGng0yloAkzWfrNqb3Sbq2VFd3jPlqxwC45XP8AwICr1Iw4oGeJ3qPLBC8Q3PCcjHcda1YrgkA9iM1r+JtLWxvjOg2w3BJ3MeN5PI/rj646Vzm8Z+VgQCRkdCK5pL7LOuL2kbMNxkAVejZHA6Z71z8ExBq/FPtOcisbWOhO5c1F1EGSfTJ9BWONTIuAsNtMyDkuF4qW91ONBt4YntjNZ1tNc+f5kakAdAo4q4xutSJStsadzrc0sZCK8rAfKoH+cVVtLu5KRmaEKxYEANuP41YmuppYVXuQfMxxg/8A6qzBcXNqScEr6VXKiXJ9TpmIK5BqnLPgEE/WqkGqJKpzx9aSZwTkd6y5Wty+dWGvIWbGagt7RrrUHWNRuZgvPf6+1GcuBXUaJpa22bppN7SDKjbjZkfqffitErIxnLW5uWMC29vFCpJCKFyRycDqfetu3GFFZEHUVsQfdFbQOeRZB4ozTRRWpBXBpc1U+1D+7Si6H92nZiLYagnNVftP+zR9p/2f1osxj5vu15/4rTF75ikHgK3qG9PyIruZLkkfdri9e0dp7truGdkJXa8bDcHGcjnPGCWx1644AAGc1oVF2ZzcchzS3NzIgCp/F3qA5STBHQ81KcNjd1rA6r6FbKxMCQXlPQVahmueMIBn1obavP606HUobeVXaNZMfw1aYlHXUkP2tuVHIHT1qs4ugSeCc9Kstr6PnbZsBkn8Pzqs960jbsYz2Aqm7FS5XsyBg7cldjVZikJgUE5akwGXOKAARWbdyLWJIBumA967+BfLiRP7qgVxWmxf6Sr4yF+bmumjvJT3H5UmQzctxzWxDwornrOeQ45H5VqJcOB1FbwRlJmlmkzVE3Mn96k+0Sf3q0sRciEHvThD71JS0XCxF5PvS+SPWmyXcERw0gJ9BzUJ1KLb8qPn0OBWE8TSh8UkaKnJ7ImMAI61RurRSCWbA9TSXGoTEHau0Hpjqfb/AOvUWS4USMWYDJy2etcNbM6aXuK5tDDSe5xOuWkVtefuwQrcjPf6VnL1Ga0/FUjPqqwdFWEOD7kn/CsJbgqdsnB9fWtKM5TgpPqW0k7F142bG3pVi1sYy2ZACapx3iAYJqVb1QeGrXUpNG2mnQomdig9s9RWfc20QJ2gCqj6kwcZbOfeo5L8EHLU3dh7pKsYjzg5FNd1jjLNwBVN75cdaqtM9w4z90dBRYhtFyz1/wCxan5U/Fu2M8dK9GhtU4yMGvKL22DkP3Ar1HS2l/siy83JlNvGWz/e2jP61x4yo6CjUh9w6ceduLNu1gjHAIz6VdWFcdKxH3mU4PyFQwyuc/rSxSTbiIj35w2CPw/Oohmv80RSw3mbvlL6UeUvpWNHqV0HKkNhQDlgMEEkcHv0/l61OmsH+KPdzj5QR3xXVHMqL3ujN4efQrnV9+QmUxjgocj6giopLi5nVtuRweC2M+nA61Wg8xk2g7R/CCOQPf8AyOlPjYJIdpJB6AHIGK8Spiqs95M6404rZCISzAE7D/FkZp00iWSiSQ/KWALf3c8f5+tSsqupZgASe1Z1410WCNBGbVWDSOWzlQcnj1496yh70rdC3oi9LmTYyMNhGQRzu/8Arf41LIGDBmIJZR0GP89ajVP3iSKXVWUHymAATgYAHb396mky0Y5AIyRz2/r/AJ/GJ72RUTkPFdttvoLnJO+Py8Y6bTn/ANm/SuekhWQYx+Nd9qdiNSsGhyBIDuRj2Yevt1H41wzLJBK0UqFHU4ZT1Br2cBWU6Sj1RhUjZmY8LRnHPtULGRehNa8iq4quYR3Fd/MZOJlmSXNOTe3UnFaBthR9nA7U+YnlKu3OABVqGPYMnrSqgXnvSlwATnpSKSsOS3kvbmO2jz5krBAQM4z3+g616fgGTCDgDoK5nw3o72h/tC7QrMykRRsOUB6sfQnp9M+vHURRyFCybd55G/pXiZjXU5qEehvRhZXZK+BcCLIDbAwBHXkjA/Kkkk2IwcFMcBiRz9P/AK9QQJNPI7XIVZkKlCmQQMnqMnj8fWo7Zp7u4d5ZVMcbFAsa7QSDgnnn1GM4/Q1zOCS3LvdijDZCMCxOMLjj3x7AfpU4QAAKAFAwAKWZiNkacF225A6cE5/SoGkkAfcSnyllAxnaD159ivWps2gIBGfMDFdvIDDGQ2QCTj6mpju+0AbQVKk7vpj/ABpCjGJkUMnJ5Zsk/jz/AJFSqX2Ngc4wMHp69RSdmBDJiTI3YjHGVYgk56ZH+e1SrCJ4Sm4NHIuNw6gHuD39qhf5mVWDJGOD8xXH4/8A1/1AzZt2LO8pOEA2qCCAPqD/AD98cEGna0bjuLI4D52lif4R3p0Zc9yMdmXnB7Z6H/8AVTYz1kYZLHAHPA/p/wDXxUgAYiN03Hbyccc9RUtDIZ4vLPmJ909RWTqekQaogbd5c69JAO3ofUVvIyleBwexPWqr2+DmPORwRUwqSpyunqU0pI4C70m/sdxmgYxjnzE+Zcevt+OKzy7KcivS/MI4IqpcafYXO7zbWMsTywXaT+I5r1aeY/zr7jB0uxwPntj7tRszP1OBXcHQNJ/59D/3+f8Axq5BY2dsVaG1hRl6PtBYfj1rV5hTSukxezZxdpomo34HkwFIyM+bL8q49u5/AGuo03w7Z6btlkP2m4ByHdcBeeML69OTnpxitfezHAqVbdtu5+PXNcNfHVKistEaRpJbkcUZlkBq6VwNsZyQRk4pirnhSAhIPTnilztWQopyFOAeOctXHGPVlt2KtxbyPqiSqzIqxAEr/Ectx6f5FFrYfYpd0Ursj/6xZDn3BB9f8farzlVGSeKarqU3849Mc/l6+1be0lt02I5URSY4G4Bifl9zURtyQdkUSeZxK4+9j245P48Zzz0MjlVYB1Bkl4IA7Dpn2HP6nHWl+eGIJuySSFYgkDnjPPOB374pJNDIM45pydaKKwQDY5EdyFOeMggcH3B6d6SN7c2xX7RGV6Ahxn8eef8A9dFFbcqTAl2ScMCjMOgOQPz/AD/SlVcKBkBn6nocf5P5nNFFK9wJvao34cNk+hx/n/PNFFQkO4wx7ySyqwPTHB+tQtbx/wC2nqSKKKW2xVxBDGP+Wp/75/8Ar0qpHgn52H+yP/10UULVgyyodT8qRrjseSf14p5fIDcnPqP6UUVdkJtiLGduCxA9uo+lOdQcKgC4XGc4AHOOB1+lFFCbJImZpQ38Kgjr+f8AP+RqQ4+VskA8fT0/z/hRRVPcRBJG6SGXLMxUDcOxz6fljPH0ycqqYCRZ4Ay4xwc9sen+AoopqTEz/9k=\",     \"prn\": null,  \"uid\": \"935452389522\",     \"ldata\": {       \"lang\": \"13\",    \"name\": \"उमेश श्यामलाल चौहान\",    \"co\": null,  \"house\": \"देवमन तिवारी चाळ\",\"street\": \"एस.व्ही.रोड\",   \"lm\": \"एमटीएनएल जवळ\", \"loc\": \"वासरी हिल, डोंगरी\",  \"vtc\": \"गोरेगाव पश्चिम\",      \"subdist\": \"मुंबई\",     \"dist\": \"मुंबई\",  \"state\": \"महाराष्ट्र\", \"pc\": \"400104\",   \"po\": null  } } }";
    }

    public void decodeImageFromBase64(View v){

        getDataFromJSON();

    }

    private void getDataFromJSON() {
        try {
            JSONObject responseObject = new JSONObject(responseFromServer);   //main string coming from server i.e. response

            JSONObject aadhardtlsObject = responseObject.getJSONObject("aadhardtls");   //fetching aadhaardtls json object from main oject

            JSONObject poiObject = aadhardtlsObject.getJSONObject("poi");      //from aadhaardtls object
            name = poiObject.getString("name");
            dob = poiObject.getString("dob");
            gender = poiObject.getString("gender");
            phone = poiObject.getString("phone");
            email = poiObject.getString("email");

            JSONObject poaObject = aadhardtlsObject.getJSONObject("poa");
            co = poaObject.getString("co");
            house = poaObject.getString("house");
            street = poaObject.getString("street");
            lm = poaObject.getString("lm");
            loc = poaObject.getString("loc");
            vtc = poaObject.getString("vtc");
            subdist = poaObject.getString("subdist");
            dist = poaObject.getString("dist");
            state = poaObject.getString("state");
            country = poaObject.getString("country");
            pc = poaObject.getString("pc");
            po = poaObject.getString("po");
            vtcCode = poaObject.getString("vtcCode");

            pht = aadhardtlsObject.getString("pht");
            prn = aadhardtlsObject.getString("prn");
            uid = aadhardtlsObject.getString("uid");

            JSONObject ldataObject = aadhardtlsObject.getJSONObject("ldata");
            lLang = ldataObject.getInt("lang");
            lName = ldataObject.getString("name");
            lCo = ldataObject.getString("co");
            lHouse = ldataObject.getString("house");
            lStreet = ldataObject.getString("street");
            lLm = ldataObject.getString("lm");
            lLoc = ldataObject.getString("loc");
            lVtc = ldataObject.getString("vtc");
            lSubdist = ldataObject.getString("subdist");
            lDist = ldataObject.getString("dist");
            lState = ldataObject.getString("state");
            lPc = ldataObject.getString("pc");
            lPo = ldataObject.getString("po");

            decodeImage(pht);

            String langName = getlanguageName(lLang);

            jsonDataTv.setText("name:"+name
                    +"\ndob:"+dob
                    +"\ngender:"+gender
                    +"\nphone:"+phone
                    +"\nemail:"+email
                    +"\n\nco:"+co
                    +"\nhouse:"+house
                    +"\nstreet:"+street
                    +"\nlm:"+lm
                    +"\nloc:"+loc
                    +"\nvtc:"+vtc
                    +"\nsubdist:"+subdist
                    +"\ndist:"+dist
                    +"\nstate:"+state
                    +"\ncountry:"+country
                    +"\npc:"+pc
                    +"\npo:"+po
                    +"\nvtcCode:"+vtcCode
                    +"\n\nprn:"+prn
                    +"\nuid:"+uid
                    +"\n\nlang:"+lLang+"\nlang name(Local language):"+langName
                    +"\nname:"+lName
                    +"\nco:"+lCo
                    +"\nhouse:"+lHouse
                    +"\nstreet:"+lStreet
                    +"\nlm:"+lLm
                    +"\nloc:"+lLoc
                    +"\nvtc:"+lVtc
                    +"\nsubdist:"+lSubdist
                    +"\ndist:"+lDist
                    +"\nstate:"+lState
                    +"\npc:"+lPc
                    +"\npo:"+lPo
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private String getlanguageName(int lLang) {
        switch (lLang){
            case 1: languageName = "Assamese";
                    break;
            case 2: languageName = "Bengali";
                    break;
            case 5: languageName = "Gujarati";
                    break;
            case 6: languageName = "Hindi";
                    break;
            case 7: languageName = "Kannada";
                    break;
            case 11: languageName = "Malayalam";
                    break;
            case 12: languageName = "Manipuri";
                    break;
            case 13: languageName = "Marathi";
                    break;
            case 15: languageName = "Oriya";
                    break;
            case 16: languageName = "Punjabi";
                    break;
            case 20: languageName = "Tamil";
                    break;
            case 21: languageName = "Telugu";
                    break;
            case 22: languageName = "Urdu";
                    break;
        }
        return languageName;
    }

    private void encodeImage(){

        //encode image to base64 string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.lighthouse);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

    }

    private void decodeImage(String imageString){

        //String imageString = "/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADIAKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDqcUuKUUooAQCnAUUtABijFFLTAMUYpadkBSSQABkmgBuKXbTRNE0fmCVDGRneGGMfWpAQQCCCCMg+ooATFGKdxmikAmKMU7FGKAG4oxTqKAG4pMU6igBmKTFPooArCnClxS4oAQUtFKBTAKXFAFVNR1aw0i2M99cpDH7gkkZAOAMk4LDOBxmgC2SB1IH1qnearZWTbJbq2jm27gsswQY9T6flXl/iH4j6jfExabmxt+RnjzWHuf4ex+XkepFcbNcT3kzS3E0s0rHLO5Lsx6ZJPJNAHrOr+P8AT7WC4S2vkkm3ER+VGVZPTO4FWHuDz6d65yy8cXzSMZvEMMQB3FpbDJb2IReemM/l7cG6FW2qHLdcYqLzSpIHy/hikB7PZeNpoty6nDby28eN9/ZSb4wSeMrkkcFe5OeMZIFdjb3MVzEskTq6soYMpyGB6EEdRXzjp+pz6ddpcQPtIJypGVYEEFSOhBBIIPY13Xh/xqmlwvFOheKU5gVTt25JySTgYz1b3PB5pgetUtedR/E2M+Xm0gDFwGH2p8AY6/6vnniuj0fxjYa1MIbZSsp48qRgrscn7o6EYBbg5wOlFgOhopwwwBByCM0YoAZRTsUmKQCUlOpMUAVxTqQCloAKUUYrL8Q6xFoWiXF9Iyh1XbCrc75CPlGM888nHYE9qYGT4u8aQ+H4zbWoSfUWH3GPyxDGQWx3OQQPTk44z49qOqXWo3bXN5cvPO/LOx6/TsBzwBgDsBUd3M9zcO7b3lkYs3OSSTkknuSSSajddsaqVQH0A5/EmkBGJCx6de5xTzI2CoY57bQKi+U+59hTxIhQr5LZH8Q6UDIg8qSlxNsYcgnP9Ka83mn5wpPqKmaVJIwrYJHt2qPyUdcgj6Z5/KgRCd4zjp7UCZgRz0p4jwe9MZCOeABQBMrsx++BVuOWVAAXyGqgmD1GR7GrPllApjOQRkBjQB6T4O8ffZJBYazO7QO5ZLt2JZGJJIcnqpJzu6g5zkH5fVFYMoYHIPIyMV8yhnKngAfyr0v4d+MW82PQNQP3vltZchQuB9w/UDg+vHcYAPT6SnYpMUAJRS4pKAK9LSUtADq8h+Juqtc6+tiGIjsowAvH33AYn8to9sH1r1qRxHGWY7VHU+lfPeqag+r63c38xOJZC+COVX+FfwUAfhQBRJCjLZz6ZqvJc5kJGce1TO7PnbjLfoKiEIBAyGJ7DtQMbEJJW+UdPariLc7skkZ71f0+3Krz0PYVrpACuNox9KxlUtsbRpXWpzP2IOxIYDuTjFN+y5JIYn3rpnsWbj5Rk8k8YqvFpciygE4Rjxzz/wDqpe00KdIxYrKRkHAOfemy2bGIuB+FdSdNCDYOvcjqaS5slWAKABgcCp9o7j9locWImBPl8nHKmrBkUQo6H5DwyZ6GrdxbCKUgr0NU54kU/JkHvnvW8ZXRzyi0PJ2LtweehzQrvA8c0TssiEMroxBUjoQexBqMAmA55x+lPibK/N071RJ9AeD9cfXtAjnnIN1C3kTkLtDsACHAwOGVlboBkkDpW9Xlvwr1I/bLnTWVz/o+5XJG0Kj52gY9ZmJ65yPQV6jQAhpKWkoArinUzNOFAGH40uJLbwdqbxY3NEIzn+67BG/RjXgqnJK9znmvePGgz4P1LIc/uwcKQD94fp6+1eCb9smfSmA7BJ2g4J4NWbS3XcGPeqofeS2MH19q0bD52BqJOyLgrs17eMAdMCtGLC471VgjJwAK0oLbpu71xyZ3RQ5IQw3c5P5CpokVSWCc9M1bhs9y8dqc1m44waLsehSlQtyBtNVJEyK1fsj9xUcsChc9al3GrHL3UIkz8uDWNNCd5Vh6gGuvuLdD3xWTeWgdCAQw/WtoSsY1IpnNNmNiuc5H5UxSRnPQ1LOrRTbX7dDUOflI7V0p9TjasehfCli3iGdfMIKWrHbt+8Cyg89ui/X8K9h614p8KpAviq4z/wA+Lj/yJHXs6tkU7gPNFJmigCsBTgKaDTqBHOePpGi8E6iVJBIjXPoDIoP5jj8a8FbgH619Ha5aw3uiXdvPBJPG0ZJij+85HIA98gYr52vLZra4mt5cCSJyjY9QcGgCJeQqDq3Wt21VowBFGXcD8KxIV/0qMetdlCFtrZfl5xk8VlUdjalG5SU6qoLLDn2yKa+ralbOC6N9NlX0vJ5Y5WijJWJSzlTgLjnk/wCAqrFqxvp44GslzI4RGLdSTj05pRTfQ0lZbSLNn4pu2YIYgFPBJB4roP7eJs1baM4yRiufk054XlGwpJEfnibqO2fce44rQ04ROCkidRWc3boaQT7kkuuXDxbljCjGc+tc7d63qDEjeR7gCtvVlWMBIwcdqq2GnCZosFGnlb5Vc4AA7n0pwlcJxZjj+0bjDPM8SnnLnANWV0pzhmvNx9VFSapdajY3c9i8xRo5QqRpFgOvPOevPGB7+1Xb/T5oIoJA++VkBkjfG5CR0DDr+NaST6GUeV+Zh6tpzQ23m+ZvAPXuKxP4c12cls91YSxMDkrxn1rj2jZCyMMHNFN3RnVjZ6HYfDE7fE87d/sT8f8AbSOvZ7d8gV5b8OLRTFPfYG5T9nHygEDhzzjnJbv6D0r061bgVV9SLaF0ciikByKWrEVBTxTBT6AF7V4j4/8AD50nxDLNDEEs7vMsW3oG/jHtzzgcYYV7dXLeO7dZ9CRvI8wpMCW/uKVYE/ido/EUPRDSu7Hi+nW/m6jCP9oV232RZl2sOO9YUdslnrEOzIR2+X+VdZCF2jJrmqu6TOmlGzaKUMc1luFuBtPVcAg/Wq9np8VreC5ht/LlUkhs8D6A5xW0EBPSp9ionAG41EZvubuCMy6VWYSFD5pHLlmJI/Emq9pEQ+fWtG5TbExPLHvVW2jct8vp1qZPm1KSsGoQ5jDelRWRwv3VYDjkcirstrK0eCRyM/WqtopimKkcGlF2QSWpNJbSso2MoUcjCAfyxTY7M7gztkjtV1MDp0NKwVRnpTlJsOUrSQ47Vx2p2BbUnjjA5YH8D/8ArrspbgDA4rm7lt+sz44Cx/0q6baTMKkbtJnYeC4HtbC4ifqLgkYHGMAdfXj8sV29qeBXJ+GI3XTDIzhvNlZxjt0Uj81J/Gustelaw1MJpJ6F1TxS0i8CnVsZFMGng0yloAkzWfrNqb3Sbq2VFd3jPlqxwC45XP8AwICr1Iw4oGeJ3qPLBC8Q3PCcjHcda1YrgkA9iM1r+JtLWxvjOg2w3BJ3MeN5PI/rj646Vzm8Z+VgQCRkdCK5pL7LOuL2kbMNxkAVejZHA6Z71z8ExBq/FPtOcisbWOhO5c1F1EGSfTJ9BWONTIuAsNtMyDkuF4qW91ONBt4YntjNZ1tNc+f5kakAdAo4q4xutSJStsadzrc0sZCK8rAfKoH+cVVtLu5KRmaEKxYEANuP41YmuppYVXuQfMxxg/8A6qzBcXNqScEr6VXKiXJ9TpmIK5BqnLPgEE/WqkGqJKpzx9aSZwTkd6y5Wty+dWGvIWbGagt7RrrUHWNRuZgvPf6+1GcuBXUaJpa22bppN7SDKjbjZkfqffitErIxnLW5uWMC29vFCpJCKFyRycDqfetu3GFFZEHUVsQfdFbQOeRZB4ozTRRWpBXBpc1U+1D+7Si6H92nZiLYagnNVftP+zR9p/2f1osxj5vu15/4rTF75ikHgK3qG9PyIruZLkkfdri9e0dp7truGdkJXa8bDcHGcjnPGCWx1644AAGc1oVF2ZzcchzS3NzIgCp/F3qA5STBHQ81KcNjd1rA6r6FbKxMCQXlPQVahmueMIBn1obavP606HUobeVXaNZMfw1aYlHXUkP2tuVHIHT1qs4ugSeCc9Kstr6PnbZsBkn8Pzqs960jbsYz2Aqm7FS5XsyBg7cldjVZikJgUE5akwGXOKAARWbdyLWJIBumA967+BfLiRP7qgVxWmxf6Sr4yF+bmumjvJT3H5UmQzctxzWxDwornrOeQ45H5VqJcOB1FbwRlJmlmkzVE3Mn96k+0Sf3q0sRciEHvThD71JS0XCxF5PvS+SPWmyXcERw0gJ9BzUJ1KLb8qPn0OBWE8TSh8UkaKnJ7ImMAI61RurRSCWbA9TSXGoTEHau0Hpjqfb/AOvUWS4USMWYDJy2etcNbM6aXuK5tDDSe5xOuWkVtefuwQrcjPf6VnL1Ga0/FUjPqqwdFWEOD7kn/CsJbgqdsnB9fWtKM5TgpPqW0k7F142bG3pVi1sYy2ZACapx3iAYJqVb1QeGrXUpNG2mnQomdig9s9RWfc20QJ2gCqj6kwcZbOfeo5L8EHLU3dh7pKsYjzg5FNd1jjLNwBVN75cdaqtM9w4z90dBRYhtFyz1/wCxan5U/Fu2M8dK9GhtU4yMGvKL22DkP3Ar1HS2l/siy83JlNvGWz/e2jP61x4yo6CjUh9w6ceduLNu1gjHAIz6VdWFcdKxH3mU4PyFQwyuc/rSxSTbiIj35w2CPw/Oohmv80RSw3mbvlL6UeUvpWNHqV0HKkNhQDlgMEEkcHv0/l61OmsH+KPdzj5QR3xXVHMqL3ujN4efQrnV9+QmUxjgocj6giopLi5nVtuRweC2M+nA61Wg8xk2g7R/CCOQPf8AyOlPjYJIdpJB6AHIGK8Spiqs95M6404rZCISzAE7D/FkZp00iWSiSQ/KWALf3c8f5+tSsqupZgASe1Z1410WCNBGbVWDSOWzlQcnj1496yh70rdC3oi9LmTYyMNhGQRzu/8Arf41LIGDBmIJZR0GP89ajVP3iSKXVWUHymAATgYAHb396mky0Y5AIyRz2/r/AJ/GJ72RUTkPFdttvoLnJO+Py8Y6bTn/ANm/SuekhWQYx+Nd9qdiNSsGhyBIDuRj2Yevt1H41wzLJBK0UqFHU4ZT1Br2cBWU6Sj1RhUjZmY8LRnHPtULGRehNa8iq4quYR3Fd/MZOJlmSXNOTe3UnFaBthR9nA7U+YnlKu3OABVqGPYMnrSqgXnvSlwATnpSKSsOS3kvbmO2jz5krBAQM4z3+g616fgGTCDgDoK5nw3o72h/tC7QrMykRRsOUB6sfQnp9M+vHURRyFCybd55G/pXiZjXU5qEehvRhZXZK+BcCLIDbAwBHXkjA/Kkkk2IwcFMcBiRz9P/AK9QQJNPI7XIVZkKlCmQQMnqMnj8fWo7Zp7u4d5ZVMcbFAsa7QSDgnnn1GM4/Q1zOCS3LvdijDZCMCxOMLjj3x7AfpU4QAAKAFAwAKWZiNkacF225A6cE5/SoGkkAfcSnyllAxnaD159ivWps2gIBGfMDFdvIDDGQ2QCTj6mpju+0AbQVKk7vpj/ABpCjGJkUMnJ5Zsk/jz/AJFSqX2Ngc4wMHp69RSdmBDJiTI3YjHGVYgk56ZH+e1SrCJ4Sm4NHIuNw6gHuD39qhf5mVWDJGOD8xXH4/8A1/1AzZt2LO8pOEA2qCCAPqD/AD98cEGna0bjuLI4D52lif4R3p0Zc9yMdmXnB7Z6H/8AVTYz1kYZLHAHPA/p/wDXxUgAYiN03Hbyccc9RUtDIZ4vLPmJ909RWTqekQaogbd5c69JAO3ofUVvIyleBwexPWqr2+DmPORwRUwqSpyunqU0pI4C70m/sdxmgYxjnzE+Zcevt+OKzy7KcivS/MI4IqpcafYXO7zbWMsTywXaT+I5r1aeY/zr7jB0uxwPntj7tRszP1OBXcHQNJ/59D/3+f8Axq5BY2dsVaG1hRl6PtBYfj1rV5hTSukxezZxdpomo34HkwFIyM+bL8q49u5/AGuo03w7Z6btlkP2m4ByHdcBeeML69OTnpxitfezHAqVbdtu5+PXNcNfHVKistEaRpJbkcUZlkBq6VwNsZyQRk4pirnhSAhIPTnilztWQopyFOAeOctXHGPVlt2KtxbyPqiSqzIqxAEr/Ectx6f5FFrYfYpd0Ursj/6xZDn3BB9f8farzlVGSeKarqU3849Mc/l6+1be0lt02I5URSY4G4Bifl9zURtyQdkUSeZxK4+9j245P48Zzz0MjlVYB1Bkl4IA7Dpn2HP6nHWl+eGIJuySSFYgkDnjPPOB374pJNDIM45pydaKKwQDY5EdyFOeMggcH3B6d6SN7c2xX7RGV6Ahxn8eef8A9dFFbcqTAl2ScMCjMOgOQPz/AD/SlVcKBkBn6nocf5P5nNFFK9wJvao34cNk+hx/n/PNFFQkO4wx7ySyqwPTHB+tQtbx/wC2nqSKKKW2xVxBDGP+Wp/75/8Ar0qpHgn52H+yP/10UULVgyyodT8qRrjseSf14p5fIDcnPqP6UUVdkJtiLGduCxA9uo+lOdQcKgC4XGc4AHOOB1+lFFCbJImZpQ38Kgjr+f8AP+RqQ4+VskA8fT0/z/hRRVPcRBJG6SGXLMxUDcOxz6fljPH0ycqqYCRZ4Ay4xwc9sen+AoopqTEz/9k=";

        //decode base64 string to image
        byte[] imageBytes = Base64.decode(imageString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        aadhaarProfileImage.setImageBitmap(decodedImage);
    }



}
